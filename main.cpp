#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "path.h"
#include "functions.h"
#define TEST_COUNT 1000

namespace fs = filesystem;

std::ofstream &operator<<(std::ofstream &fout, std::vector<int> &v) {
    for (int i = 0; i < v.size(); ++i)
        fout << v[i] << ' ';
    fout << std::endl;

    return fout;
}

int read_vector(std::vector<int> &v, int size, std::ifstream &fin) {
    for (int i = 0; i < size; ++i) {
        int elem;
        fin >> elem;
        v.push_back(elem);
    }
}

int main() {
    fs::path di("debuginfo");
    if (!di.exists())
        create_directory(di);

    std::ofstream res("debuginfo/res.txt");
    freopen("debuginfo/err.log", "w", stderr);

    size_t WA_count = 0;

    for (size_t i = 1; i <= 1000; ++i) {
        std::ifstream test("tests/test" + std::to_string(i) + ".txt");
        
        int size;
        test >> size;

        std::vector<int> uv;
        read_vector(uv, size, test);
        
        test.close();

        std::ifstream ans("arrs/arr" + std::to_string(i) + ".txt");

        std::vector<int> ov;
        read_vector(ov, size, ans);
        
        ans.close();

        quickSort(uv.begin(), uv.end());

        res << i << "\t:";
        if (uv == ov)
            res << " OK" << std::endl;
        else {
            res << " WA" << std::endl;
            ++WA_count;
        }
    }

    res << "______________" << std::endl;
    res << "Right: " << TEST_COUNT - WA_count << " Wrong: " << WA_count;

    res.close();

    return 0;
}
