//
// Created by stephen on 24.02.17.
//

#ifndef MAIN_COMPARATOR_H
#define MAIN_COMPARATOR_H

struct Less {
    template<typename T>
    bool operator()(T l, T r) {
        return l < r;
    }
};

struct Greater {
    template<typename T>
    bool operator()(T l, T r) {
        return l > r;
    }
};

struct notLess {
    template<typename T>
    bool operator()(T l, T r) {
        return l >= r;
    }
};

struct notGreater {
    template<typename T>
    bool operator()(T l, T r) {
        return l <= r;
    }
};

struct Equal {
    template <typename T>
    bool operator()(T l, T r) {
        return l == r;
    }
};

#endif //MAIN_COMPARATOR_H
