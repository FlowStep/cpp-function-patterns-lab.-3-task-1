//
// Created by stephen on 21.02.17.
//

#ifndef CPP_FUNCTION_PATTERNS_LAB_3_TASK_1_FUNCTIONS_H
#define CPP_FUNCTION_PATTERNS_LAB_3_TASK_1_FUNCTIONS_H

#include <algorithm>
#include <cassert>
#include "comparator.h"

template<typename iterator, typename comparator = Less>
void quickSort(iterator begin, iterator end, comparator rel = Less()) {
    auto key = *(begin + (end - begin) / 2);

    iterator l = begin;
    iterator r = end - 1;

    while (l <= r) {
        while (rel(*l, key))
            ++l;
        while (rel(key, *r))
            --r;

        if (l <= r)
            std::swap(*l++, *r--);
    }

    if (r > begin) quickSort(begin, r + 1, rel);
    if (l < end) quickSort(l, end, rel);
}

template <typename iterator, typename valueType, typename comparator = Equal>
iterator linearSearch(iterator begin, iterator end, valueType key, comparator rel = Equal()) {
    for (iterator i = begin; i < end; ++i)
        if (rel(*i, key))
            return i;

    return end;
};

template <typename iterator, typename valueType, typename comparator = Equal>
int countOf(iterator begin, iterator end, valueType key, comparator rel = Equal()) {
    int count = 0;
    for (iterator i = begin; i < end; ++i)
        if (rel(*i, key))
            ++count;

    return count;
}

template <typename iterator, typename comparator>
iterator theMost(iterator begin, iterator end, comparator rel) {
    iterator most = begin;

    for (iterator i = begin + 1; i < end; ++i)
        if (rel(*i, *most))
            most = i;

    return most;
}


#define maxElement(begin, end) theMost(begin, end, Greater())
#define minElement(begin, end) theMost(begin, end, Less())

template <typename iterator, typename valueType, typename comparatorOrder = Greater, typename comparatorValue = Equal>
iterator binarySearch(iterator begin, iterator end, valueType key, comparatorOrder relOrder = Greater(), comparatorValue relVal = Equal()) {
    iterator l = begin;
    iterator r = end - 1;

    if (relOrder(key, *r) || relOrder(*l, key))
        return end;

    while (l < r) {
        iterator m = l + (r - l) / 2;

        if (!relOrder(key, *m))
            r = m;
        else
            l = m + 1;
    }

    if (relVal(*r, key))
        return r;

    return end;
}

template <typename iterator, typename comparatorOrder = notLess>
bool isOrderedBy(iterator begin, iterator end, comparatorOrder relOrder = notLess()) {
    for (iterator i = begin + 1; i < end; ++i) {
        if (!relOrder(*i, *(i - 1)))
            return false;
    }

    return true;
}

#define isStrictlyIncreasing(begin, end) isOrderedBy(begin, end, Greater())
#define isStrictlyDecreasing(begin, end) isOrderedBy(begin, end, Less())
#define isIncreasing(begin, end) isOrderedBy(begin, end, notLess())
#define isDecreasing(begin, end) isOrderedBy(begin, end, notGreater())

template <typename iterator>
bool isOrdered(iterator begin, iterator end) {
    return isIncreasing(begin, end) || isDecreasing(begin, end);
}

template <typename iterator, typename comparator = Greater>
void merge(iterator lb, iterator le, iterator rb, iterator re, iterator mb, iterator me, comparator rel = Greater()) {
    iterator li = lb;
    iterator ri = rb;
    iterator mi = mb;

    assert(le - lb + re - rb <= mb - me);

    while (li < le && ri < re) {
        if (rel(*li, *ri))
            *mi++ = *ri++;
        else
            *mi++ = *li++;
    }
    while (li < le)
        *mi++ = li++;
    while (ri < re)
        *mi++ = ri++;
}

#endif //CPP_FUNCTION_PATTERNS_LAB_3_TASK_1_FUNCTIONS_H
