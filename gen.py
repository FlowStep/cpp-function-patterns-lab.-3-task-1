import random
import os

num = 1

def aprint(arr):
    global num
    answer = open('arrs/arr' + str(num) + '.txt', 'w')
    for elem in arr:
        answer.write(str(elem) + ' ')
    random.shuffle(arr)
    answer.close()
    test = open('tests/test' + str(num) + '.txt', 'w')
    test.write(str(len(arr)) + ' ')
    for elem in arr:
        test.write(str(elem) + ' ')
    test.close()
    num += 1

if ('tests' not in os.listdir()):
    os.mkdir('tests')
if ('arrs'  not in os.listdir()):
    os.mkdir('arrs')

for i in range(1, 1001):
    gen_arr = list(range(1, 100001, 1 * i))
    n = len(gen_arr) // 2
    for j in range(n):
        pos = random.randint(1, len(gen_arr) - 1)
        gen_arr[pos - 1] = gen_arr[pos]
    aprint(gen_arr)
